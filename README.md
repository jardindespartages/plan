# Plan du Jardin des Part'âges

Le [plan global](./plan_global.svg) sert de "fond de carte", pour la réalisation d'autres plans plus spécifiques.
Il a été pensé pour:
 - être imprimé 
    - sur un format A3
    - ou éventuellement sur 2 format A4, chaque page représentant une des "rues" du jardin
 - être copié et modifié numériquement

La forme générale du jardin et de ses alentours proviennent essentiellement des données du [Cadastre](https://www.cadastre.gouv.fr/scpc/rechercherPlan.do).

Les mesures proviennent de mesures réalisées sur le terrain à l'aide d'un mètre et d'un décamètre ruban.
> La [vue aérienne de Google Maps](https://www.google.fr/maps/search/jardin+des+partges/@45.7659015,4.8623469,86m/data=!3m1!1e3?hl=en) peut aussi aider. 

Si vous constatez un problème ou une inexactitude, [créer un nouveau ticket](https://framagit.org/jardindespartages/plan/issues/new).

## Version du logiciel

- Inkscape `0.92.4`

## Utilisation

Ouvrir le fichier `plan_global.svg` dans le logiciel Inkscape.

### Création de vues spécifiques

> Voir par exemple [grande_parcelle_et_bacs.pdf](./grande_parcelle_et_bacs.pdf)

Depuis `Fichier > Propriétés du document` il est possible de choisir le taille de la page (par exemple A4):
![](./doc/proprietes_document.png "")

Depuis le menu calque il est possible de masquer des groupes d'élements:

![](./doc/calques.png "")

Effectuer vos rotations, translations et autres mises à l'échelle des élements désirés jusqu'à obtenir le résultat voulu.
> Pour plus d'informations sur l'utilisation du logiciel Inkscape: https://inkscape.org/fr/apprendre/

Enfin vous pouvez imprimer votre fond de plan si vous le désirez depuis `Fichier > Imprimer`.


## Impression

Grâce au logiciel `pdfposter` il est possible d'imprimer le plan vers des formats plus grands.

Par exemple :
```
pdfposter -mA4 -pA0 plan_global.pdf plan_global_A4_A0.pdf
```
Cette commande créee un document pdf composé de 16 pages qui une fois assemblé représenteront une version A0 du plan
